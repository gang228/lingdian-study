package com.gang.lingdian.load;

import sun.misc.Launcher;

import java.net.URL;

/**
 * @author gyg
 * @date 2021/11/11 20:17
 * desc 类加载器
 */
public class TestJDKClassLoader {

    public static void main(String[] args) {
        System.out.println(String.class.getClassLoader());
        System.out.println(com.sun.crypto.provider.DESKeyFactory.class.getClassLoader());
        System.out.println(TestJDKClassLoader.class.getClassLoader());
        System.out.println();

        ClassLoader appClassLoader = ClassLoader.getSystemClassLoader();
        ClassLoader expClassLoader = appClassLoader.getParent();
        ClassLoader bootstrapLoader = expClassLoader.getParent();
        System.out.println("bootstrapLoader = " + bootstrapLoader);
        System.out.println("expClassLoader = " + expClassLoader);
        System.out.println("appClassLoader = " + appClassLoader);
        System.out.println();

        System.out.println("bootstrapLoader 加载以下文件");
        URL[] urLs = Launcher.getBootstrapClassPath().getURLs();
        for (int i = 0; i < urLs.length; i++) {
            System.out.println(urLs[i]);
        }

        System.out.println();
        System.out.println("extClassloader加载以下文件：");
        System.out.println(System.getProperty("java.ext.dirs"));

        System.out.println();
        System.out.println("appClassLoader加载以下文件：");
        System.out.println(System.getProperty("java.class.path"));
    }

}
