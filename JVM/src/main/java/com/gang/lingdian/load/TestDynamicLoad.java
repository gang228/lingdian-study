package com.gang.lingdian.load;

/**
 * @author gyg
 * @date 2021/11/11 20:16
 * desc
 */
public class TestDynamicLoad {

    static {
        System.out.println("********* 类静态代码块**********");
    }

    public static void main(String[] args) {
        new A();
        System.out.println("******** 执行mian方法");
        B b = new B();
    }

}

class A {
    static {
        System.out.println("********* A类静态代码块");
    }
    public A(){
        System.out.println("******* A 无参构造*******");
    }
}

class B {
    static {
        System.out.println("******** B 类静态代码块********");
    }

    public B(){
        System.out.println("*******B类无参构造");
    }

}


