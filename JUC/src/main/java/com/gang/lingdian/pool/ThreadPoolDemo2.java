package com.gang.lingdian.pool;

import java.util.concurrent.*;

/**
 * @author gaoyonggang
 * @date 2021/11/6 14:50
 * desc 自定义线程池
 *
 */
public class ThreadPoolDemo2 {

    public static void main(String[] args) {
        ExecutorService threadPool = new ThreadPoolExecutor(
                2,
                5,
                3L,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(3),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());

        try {
            for (int  i = 0; i < 10; i++) {
                threadPool.execute(()->{
                    System.out.println(Thread.currentThread().getName()+"办理业务");
                });
            }
        } finally {
            threadPool.shutdown();
        }
    }
}
