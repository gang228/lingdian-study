package com.gang.lingdian.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author gaoyonggang
 * @date 2021/11/5 17:50
 * desc 线程池
 */
public class ThreadPoolDemo {

    public static void main(String[] args) {
        /**
         * 创建固定线程池
         */
        // ExecutorService threadPool1 = Executors.newFixedThreadPool(5);

        /**
         * 创建一个单线程池
         */
        // ExecutorService threadPool2 = Executors.newSingleThreadExecutor();

        /**
         * 创建一个可扩容的线程池
         */
        ExecutorService threadPool3 = Executors.newCachedThreadPool();
        try {
            for (int i = 1; i <=20 ; i++) {
                threadPool3.submit(()->{
                    System.out.println(Thread.currentThread().getName()+"办理业务");
                });
            }
        } finally {
            threadPool3.shutdown();
        }

    }

}
