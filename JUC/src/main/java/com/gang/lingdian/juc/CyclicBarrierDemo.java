package com.gang.lingdian.juc;

import java.util.concurrent.CyclicBarrier;

// 集齐7颗龙珠召唤神龙
public class CyclicBarrierDemo {

    private static final int num = 7;

    public static void main(String[] args) {
        // 创建CyclicBarrier对象
        CyclicBarrier cyclicBarrier = new CyclicBarrier(num,()->{
            System.out.println("集齐龙珠召唤神龙");
        });
        for (int i = 1; i <= 7; i++) {
            new Thread(()->{
                try {
                    System.out.println(Thread.currentThread().getName()+"星龙珠");
                    cyclicBarrier.await();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            },String.valueOf(i)).start();
        }

    }

}
