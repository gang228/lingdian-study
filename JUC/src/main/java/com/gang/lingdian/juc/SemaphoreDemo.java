package com.gang.lingdian.juc;

import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

// 模拟6辆车  抢占3个停车位
public class SemaphoreDemo {

    public static void main(String[] args) {
        // 创建Semaphore 对象
        Semaphore semaphore = new Semaphore(3);

        for (int i = 1; i <= 6; i++) {
            new Thread(()->{
                try {
                    // 抢占停车位
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName()+"抢占了停车位");
                    // 设置随机停车时间
                    TimeUnit.SECONDS.sleep(new Random().nextInt(10));

                    System.out.println(Thread.currentThread().getName()+"离开了停车位");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    // 释放
                    semaphore.release();
                }
            },String.valueOf(i)).start();
        }
    }

}
