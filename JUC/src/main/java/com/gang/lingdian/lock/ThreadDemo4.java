package com.gang.lingdian.lock;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author gaoyonggang
 * @date 2021/11/2 21:07
 * desc 演示线程不安全
 */
public class ThreadDemo4 {

    public static void main(String[] args) {
        /**
         * 演示ArrayList 不安全及解决方案
         */
        // List<String> list = new ArrayList();
        // 1. 通过Vector 解决List线程安全
        // List<String> list = new Vector<>();

        // 2. 通过JUC 的Collections的工具类
        // List<String> list = Collections.synchronizedList( new ArrayList<>());

        // 3.CopyOnWriteArrayList解决
        // List<String> list = new CopyOnWriteArrayList<>();
        // for (int i = 0; i < 30; i++) {
        //     new Thread(()->{
        //         list.add(UUID.randomUUID().toString().substring(0, 8));
        //         System.out.println(list);
        //     },String.valueOf(i)).start();
        // }


        /**
         * 演示HashSet 不安全 及解决方案
         */
        // Set set = new HashSet();

        // CopyOnWriteArraySet 解决
        // Set<String> set = new CopyOnWriteArraySet<>();
        // for (int i = 0; i < 30; i++) {
        //     new Thread(()->{
        //         set.add(UUID.randomUUID().toString().substring(0, 8));
        //         System.out.println(set);
        //     },String.valueOf(i)).start();
        // }


        /**
         * 演示HashMap 不安全及解决
         */
        //不安全
        // Map<String, String> map = new HashMap<>();

        //ConcurrentHashMap
        Map<String, String> map = new ConcurrentHashMap<>();
        for (int i = 0; i < 30; i++) {
            String key = String.valueOf(i);
            new Thread(()->{
                map.put(key,UUID.randomUUID().toString().substring(0, 8));
                System.out.println(map);
            },String.valueOf(i)).start();
        }
    }

}
