package com.gang.lingdian.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author gyg
 * @version 1.0
 * @date 2021/8/28 下午2:55
 * @Description:
 */

class LTicket{

    private int num = 30;
    Lock lock = new ReentrantLock();
    public void sale(){
        //加锁
        lock.lock();
        try {
            if(num>0){
                System.out.println(Thread.currentThread().getName()+"：卖出第："+(num--)+"，剩余："+num);
            }
        }finally {
            // 释放锁
            lock.unlock();
        }
    }
}

public class LSaleTicket {

    public static void main(String[] args) {
        LTicket lTicket = new LTicket();
        new Thread(()->{
            for (int i = 0; i < 40; i++) {
                lTicket.sale();
            }
        },"AA").start();
        new Thread(()->{
            for (int i = 0; i < 40; i++) {
                lTicket.sale();
            }
        },"BB").start();
        new Thread(()->{
            for (int i = 0; i < 40; i++) {
                lTicket.sale();
            }
        },"CC").start();
    }


}
