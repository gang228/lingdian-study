package com.gang.lingdian.syncronized;

/**
 * @author gyg
 * @version 1.0
 * @date 2021/8/28 上午10:28
 * @Description:
 */

//第一步   创建资源类，定义属性和方法
class Ticket{
    //票数
    private int num = 30;

    public synchronized void sale(){
        if(num>0){
            System.out.println(Thread.currentThread().getName()+"：卖出："+(num--)+",剩余："+num);
        }
    }
}

public class SaleTicket {

    // 第二步  创建多线程，调用资源类
    public static void main(String[] args) {
        Ticket ticket = new Ticket();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 40; i++) {
                    ticket.sale();
                }
            }
        },"AA").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 40; i++) {
                    ticket.sale();
                }
            }
        },"BB").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 40; i++) {
                    ticket.sale();
                }
            }
        },"CC").start();
    }
}
