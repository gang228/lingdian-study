package com.gang.lingdian.completable;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class CompletableFutureDemo {

    public static void main(String[] args) throws Exception {
        //异步调用无返回值
        CompletableFuture<Void> completableFuture1 = CompletableFuture.runAsync(()->{
            System.out.println(Thread.currentThread().getName()+"completableFuture1");
        });
        completableFuture1.get();

        //异步调用有返回值
        CompletableFuture<Integer> completableFuture2 = CompletableFuture.supplyAsync(()->{
            System.out.println(Thread.currentThread().getName()+"completableFuture2");
            // 模拟异常
            // int i= 1/0;
            return 1024;
        });
        Integer integer = completableFuture2.whenComplete((t, u) -> {
            //返回值
            System.out.println("-----t=" + t);
            //异常
            System.out.println("-----u=" + u);
        }).get();
        System.out.println(integer);


    }
}
