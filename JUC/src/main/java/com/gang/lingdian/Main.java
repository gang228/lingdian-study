package com.gang.lingdian;

/**
 * @author gyg
 * @version 1.0
 * @date 2021/8/17 下午10:37
 * @Description: 用户线程和守护线程演示
 */
public class Main {


    /**
     * 用户线程：自定义线程(主线程结束，用户线程还在执行，jvm存活)
     * 守护线程：比如垃圾回收(没有用户线程，都是守护线程，jvm结束)
     * @param args
     */
    public static void main(String[] args) {
        Thread aa = new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "::" + Thread.currentThread().isDaemon());
            while (true) {

            }
        }, "aa");
        // 设置为守护线程
        aa.setDaemon(true);
        aa.start();
        System.out.println(Thread.currentThread().getName()+"::over");
    }
}
