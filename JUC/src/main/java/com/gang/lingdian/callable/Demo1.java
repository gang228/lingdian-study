package com.gang.lingdian.callable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

class MyThread1 implements Runnable{
    @Override
    public void run() {
    }
}

class MyThread2 implements Callable{

    @Override
    public Object call() throws Exception {
        return 228;
    }
}


public class Demo1 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // 通过Runable接口
        new Thread(new MyThread1(),"AA").start();

        FutureTask<Integer> futureTask1 = new FutureTask<>(new MyThread2());

        // lam表达式
        FutureTask<Integer> futureTask2 = new FutureTask<>(()->{
            System.out.println(Thread.currentThread().getName()+"come in");
            return 1024;
        });

        new Thread(futureTask2,"luck").start();

        while (!futureTask2.isDone()){
            System.out.println("wait...");
        }
        // 调用FutureTask的get方法
        Integer integer = futureTask2.get();
        System.out.println(integer);

        System.out.println(Thread.currentThread().getName()+"come over");

    }
}
