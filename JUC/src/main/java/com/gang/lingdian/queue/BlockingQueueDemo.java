package com.gang.lingdian.queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author gaoyonggang
 * @date 2021/11/4 21:08
 * desc 阻塞队列
 */

public class BlockingQueueDemo {

    public static void main(String[] args) throws InterruptedException {
        // 创建阻塞队列
        BlockingQueue<String> queue = new ArrayBlockingQueue<>(3);

        /**
         * 第一组 抛出异常
         * add(E e)
         *  成功返回 true
         *  当超过队列长度 抛出异常{@link IllegalStateException} Queue full
         * remove()
         *  队列为空 抛出异常{@link java.util.NoSuchElementException}
         * remove(Object o)
         *  存在返回true
         *  不存在返回false
         * element()
         *  返回队列的头
         *  队列为空抛出异常{@link java.util.NoSuchElementException}
         */
        // System.out.println(queue.add("a"));
        // System.out.println(queue.add("b"));
        // System.out.println(queue.add("c"));
        // System.out.println(queue.element());
        // System.out.println(queue.remove("a"));
        // System.out.println(queue.remove("b"));
        // System.out.println(queue.remove("c"));
        // System.out.println(queue.remove());

        /**
         * 第二组 特殊值
         * offer(E e)
         *  成功返回 true
         *  失败返回 false
         * poll()
         *  队列不为空返回队列头
         *  队列为空返回null
         *  peek()
         *  队列不为空返回队列头
         *  队列为空返回null
         */
        // System.out.println(queue.offer("a"));
        // System.out.println(queue.offer("b"));
        // System.out.println(queue.offer("c"));
        // System.out.println(queue.offer("d"));
        //
        // System.out.println(queue.poll());
        // System.out.println(queue.poll());
        // System.out.println(queue.poll());
        // System.out.println(queue.poll());
        // System.out.println(queue.peek());

        /**
         * 第三组 阻塞
         * put(E e)
         * 当队列满时，阻塞队列
         * take()
         * 当队列为空是，阻塞队列
         */
        // queue.put("a");
        // queue.put("b");
        // queue.put("c");
        // queue.put("d");
        //
        // System.out.println(queue.take());
        // System.out.println(queue.take());
        // System.out.println(queue.take());
        // System.out.println(queue.take());

        /**
         * 第四组 超时
         * offer(E e, long timeout, TimeUnit unit)
         *  队列满时，超时返回false
         * poll(long timeout, TimeUnit unit)
         *  队列为空时，超时返回null
         */
        System.out.println(queue.offer("a", 3L, TimeUnit.SECONDS));
        System.out.println(queue.offer("b", 3L, TimeUnit.SECONDS));
        System.out.println(queue.offer("c", 3L, TimeUnit.SECONDS));
        System.out.println(queue.offer("d", 3L, TimeUnit.SECONDS));

        System.out.println(queue.poll(3l,TimeUnit.SECONDS));
        System.out.println(queue.poll(3l,TimeUnit.SECONDS));
        System.out.println(queue.poll(3l,TimeUnit.SECONDS));
        System.out.println(queue.poll(3l,TimeUnit.SECONDS));


    }

}
