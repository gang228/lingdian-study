package com.gang.lingdian.bio;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author gyg
 * @date 2022/11/13 10:54
 * desc BIO 服务端
 *
 */
public class SocketServer {


    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(9000);
        while (true){
            System.out.println("等待连接。。。");
            Socket accept = serverSocket.accept();
            handle(accept);
            System.out.println("有客户端连接了。。。");
        }
    }

    private static void handle(Socket clientSocket) throws IOException {
        byte[] bytes = new byte[1024];
        System.out.println("准备read...");
        int read = clientSocket.getInputStream().read(bytes);
        System.out.println("读取完毕。。。");
        if(read!=-1){
            System.out.println("接收到客户端数据"+new String(bytes,0,read));
        }
        clientSocket.getOutputStream().write("Hello Client".getBytes());
        clientSocket.getOutputStream().flush();

    }

}
