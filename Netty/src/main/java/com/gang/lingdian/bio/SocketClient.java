package com.gang.lingdian.bio;

import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * @author gyg
 * @date 2022/11/13 11:04
 * desc 客户端
 */
public class SocketClient {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost",9000);
        //向服务端发送消息
        socket.getOutputStream().write("Hello Server".getBytes(StandardCharsets.UTF_8));
        socket.getOutputStream().flush();
        System.out.println("向服务端发送数据结束");
        byte[] bytes = new byte[1024];
        // 接收服务端回传数据
        int read = socket.getInputStream().read(bytes);
        System.out.println("接收到服务端消息"+new String(bytes,0,read));
        socket.close();
    }
}
